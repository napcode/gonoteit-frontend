import { mapMutations } from 'vuex'

export const DisableLoadingOnUpdatedMixin = {
  methods: {
    ...mapMutations('app', ['endTask']),
  },
  updated() {
    this.$nextTick(() => {
      this.endTask('start-view')
    })
  },
}

export const DisableLoadingOnMountedMixin = {
  methods: {
    ...mapMutations('app', ['endTask']),
  },
  mounted() {
    this.$nextTick(() => {
      this.endTask('start-view')
    })
  },
}
