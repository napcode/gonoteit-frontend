import api from '@/api'
import { mapMutations } from 'vuex'
import { RecurringNoteGroup } from '@/enums.js'

export const EntityModalMixin = {
  data() {
    return {
      newNote: true,
      title: '',
      content: '',
      noteId: null,
      datetime: null,
      timestamp: null,
      error: '',
      cancelString: ' (odwołane)',
    }
  },
  methods: {
    ...mapMutations('notes', [
      'addNote',
      'addNotes',
      'updateNote',
      'updateNotes',
    ]),
    closeModal() {
      this.error = ''
      this.close()
    },
    create() {
      this.error = ''
      const noteData = this.getNoteData()
      const replace = this.getReplace()
      api
        .addNote(noteData, { replace })
        .then(response => {
          if ('errors' in response.data) {
            this.error = response.data.errors[0].message
          } else {
            const createEntity = response.data.data.createEntity
            if (createEntity.relatedEntities) {
              this.addNotes(createEntity.relatedEntities)
            }
            this.addNote(createEntity.entity)
            this.closeModal()
          }
        })
        .catch(error => {
          this.error = error
        })
    },
    update() {
      this.error = ''
      const replace = this.getReplace()
      api
        .updateNote(
          {
            id: this.args.note.id,
            ...this.getNoteData(),
          },
          { replace },
        )
        .then(response => {
          if ('errors' in response.data) {
            this.error = response.data.errors[0].message
          } else {
            const updateEntity = response.data.data.updateEntity
            if (updateEntity.relatedEntities) {
              this.updateNotes(updateEntity.relatedEntities)
            }
            this.updateNote(updateEntity.entity)
            this.closeModal()
          }
        })
        .catch(error => {
          this.error = error
        })
    },
    submit() {
      if (this.newNote) {
        this.create()
      } else {
        this.update()
      }
    },
    cancel() {
      this.title += this.cancelString
    },
    undo_cancel() {
      const new_end = this.title.length - this.cancelString.length
      this.title = this.title.slice(0, new_end)
    },
    getReplace() {
      return [
        { pattern: '#{type}', value: this.noteType.name },
        { pattern: '#{apply_to}', value: RecurringNoteGroup.THIS.name },
      ]
    },
  },
  computed: {
    isCancelled() {
      return this.title.endsWith(this.cancelString)
    },
    isActive() {
      return this['modal/isOpen'](this.modalName)
    },
    errorMessageClassObject() {
      return {
        message: true,
        'is-danger': true,
        'is-hidden': this.error === '',
      }
    },
  },
  watch: {
    name(value) {
      if (value === this.modalName) {
        this.newNote = this.args === undefined || !('note' in this.args)
        this.title = this.newNote ? '' : this.args.note.data.title
        this.content = this.newNote ? '' : this.args.note.data.content
        this.noteId = this.newNote ? null : this.args.note.id
        this.datetime =
          this.newNote || this.args.note.date === null
            ? null
            : new Date(this.args.note.date * 1000).toISOString()
      }
    },
    datetime(value) {
      if (value !== null) {
        const date = new Date(this.datetime)
        this.timestamp = Math.floor(date / 1000)
      } else {
        this.timestamp = null
      }
    },
  },
}
