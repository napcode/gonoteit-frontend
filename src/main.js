import './scss/mybulma.scss'
import '../node_modules/@fortawesome/fontawesome-free/css/all.min.css'

import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.use(Datetime)

router.beforeEach((to, from, next) => {
  store.commit('app/startTask', 'start-view')
  store.commit('notes/setDefaultTop')
  store.dispatch('notes/getChangelog')
  setTimeout(() => {
    next()
  }, 25)
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
