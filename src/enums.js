export const NoteType = {
  NONE: { name: 'NONE', value: 0 },
  NOTE: { name: 'NOTE', value: 1 },
  LIST: { name: 'LIST', value: 2 },
  CALENDAR_NOTE: { name: 'CALENDAR_NOTE', value: 3 },
  RECURRING_NOTE: { name: 'RECURRING_NOTE', value: 4 },
}

export const RecurringNoteGroup = {
  THIS: { name: 'THIS', value: 0 },
  ALL: { name: 'ALL', value: 1 },
  THIS_AND_ALL_NEXT: { name: 'THIS_AND_ALL_NEXT', value: 2 },
  ALL_NEXT: { name: 'ALL_NEXT', value: 3 },
  THIS_AND_ALL_PREVIOUS: { name: 'THIS_AND_ALL_PREVIOUS', value: 4 },
  ALL_PREVIOUS: { name: 'ALL_PREVIOUS', value: 5 },
}
