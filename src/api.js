import axios from 'axios'

function setServer(server) {
  window.localStorage.setItem('server', server)
}

function getServer() {
  return window.localStorage.getItem('server')
}

function setToken(token) {
  window.localStorage.setItem('token', token)
}

function getToken() {
  return window.localStorage.getItem('token')
}

function removeToken() {
  return window.localStorage.removeItem('token')
}

function isLoggedIn() {
  return window.localStorage.getItem('token') !== null
}

function apiCall(baseQuery, apiConfig = {}) {
  return (variables, config = {}) => {
    let query = baseQuery
    if ('replace' in config) {
      config.replace.forEach(({ pattern, value }) => {
        query = query.replace(pattern, value)
        //console.log(pattern, value, query)
      })
    }
    const data = { query }
    if (variables !== undefined) {
      data.variables = variables
    }

    const server = 'server' in config ? config.server : getServer()
    const headers = {
      Authorization: `JWT ${getToken()}`,
    }

    if ('authToken' in apiConfig && apiConfig.authToken === false) {
      delete headers['Authorization']
    }

    return axios({
      method: 'post',
      url: `${server}/graphql/`,
      data,
      headers,
    })
  }
}

const gql = {
  tokenAuth: `
    mutation ($username: String!, $password: String!){
      tokenAuth (username: $username, password: $password) {
        token
      }
    }
  `,
  allEntities: `
    query {
      allEntities{
        id,
        type,
        data,
        date
      },
      timestamp
    }
  `,
  favoriteEntities: `
    query {
      favoriteEntities{
        id,
        type,
        data,
        date
      }
    }
  `,
  calendarEntities: `
    query($dateFrom: Int, $dateTo: Int) {
      calendarEntities(dateFrom: $dateFrom, dateTo: $dateTo){
        id,
        type,
        data,
        date
      }
    }
  `,
  createEntity: `
    mutation($data: String!, $date: Int) {
      createEntity(data: $data, date: $date, type: #{type}) {
        ok,
        entity {
          id,
          type,
          data,
          date
        },
        relatedEntities {
          id,
          type,
          data,
          date
        }
      }
    }
  `,
  updateEntity: `
    mutation($id: Int!, $fields: [String], $data: String, $date: Int) {
      updateEntity(id: $id, fields: $fields, data: $data, date: $date, applyTo: #{apply_to}) {
        ok,
        entity {
          id,
          type,
          data,
          date
        }
        relatedEntities {
          id,
          type,
          data,
          date
        }
      }
    }
  `,
  deleteEntity: `
    mutation($id: Int!) {
      deleteEntity(id: $id, applyTo: #{apply_to}) {
        deleted,
        relatedEntities
      }
    }
  `,
  user: `
    query {
      user{
        id,
        data
      }
    }
  `,
  updateUser: `
    mutation($data: String, $fields: [String]) {
      updateUser(data: $data, fields: $fields) {
        ok,
        user{
          id,
          data
        }
      }
    }
  `,
  changelog: `
    mutation($timestamp: Int!) {
      changelog(timestamp: $timestamp) {
        created {
          id,
          type,
          data,
          date
        },
        updated {
          id,
          type,
          data,
          date
        },
        deleted,
        timestamp
      }
    }
  `,
}

export default {
  login: apiCall(gql.tokenAuth, { authToken: false }),
  getNotes: apiCall(gql.allEntities),
  getFavorite: apiCall(gql.favoriteEntities),
  getCalendar: apiCall(gql.calendarEntities),
  addNote: apiCall(gql.createEntity),
  updateNote: apiCall(gql.updateEntity),
  deleteNote: apiCall(gql.deleteEntity),
  getUser: apiCall(gql.user),
  updateUser: apiCall(gql.updateUser),
  getChangelog: apiCall(gql.changelog),
  setServer,
  getServer,
  setToken,
  removeToken,
  isLoggedIn,
}
