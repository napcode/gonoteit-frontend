import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Notes from './views/Notes.vue'
import Favorite from './views/Favorite.vue'
import Calendar from './views/Calendar.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/notes',
      name: 'notes',
      component: Notes,
    },
    {
      path: '/favorite',
      name: 'favorite',
      component: Favorite,
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar,
    },
  ],
})
