const state = {
  name: '',
  args: undefined,
}

const getters = {
  isOpen: state => name => {
    return state.name === name
  },
}

const actions = {}

const mutations = {
  open(state, { name, args }) {
    state.name = name
    state.args = args
  },
  close(state) {
    state.name = ''
    state.args = undefined
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
