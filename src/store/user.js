import api from '@/api'

const state = {
  isLoggedIn: false,
  data: {},
}

const getters = {
  noteIsFavorite(state, getters) {
    return noteId => getters.favoriteNotes.includes(noteId)
  },
  favoriteNotes(state) {
    return 'favorite' in state.data ? state.data.favorite : []
  },
}

const actions = {
  login({ commit }, token) {
    api.setToken(token)
    commit('setLoggedIn', true)
  },
  logout({ commit, dispatch }) {
    api.removeToken()
    dispatch('app/clearCache', null, { root: true })
    commit('setLoggedIn', false)
  },
  getUserData({ commit }) {
    api
      .getUser()
      .then(response => {
        if ('errors' in response.data) {
          commit('setLoggedIn', false)
        } else {
          commit('setLoggedIn', true)
          commit('setData', response.data.data.user.data)
        }
      })
      .catch(error => {
        commit('setLoggedIn', false)
        console.error(error)
      })
  },
  updateUserData({ commit }, { data, fields }) {
    api
      .updateUser({ data, fields })
      .then(response => {
        if ('errors' in response.data) {
          console.error(response.data.errors)
        } else {
          commit('setData', response.data.data.updateUser.user.data)
        }
      })
      .catch(error => {
        commit('setLoggedIn', false)
        console.error(error)
      })
  },
  likeNote({ dispatch, getters }, noteId) {
    const favoriteNotes = [...getters.favoriteNotes, noteId]

    dispatch('updateUserData', {
      data: JSON.stringify({ favorite: favoriteNotes }),
      fields: ['favorite'],
    })
  },
  dislikeNote({ dispatch, getters }, noteId) {
    const favoriteNotes = getters.favoriteNotes.filter(id => id !== noteId)

    dispatch('updateUserData', {
      data: JSON.stringify({ favorite: favoriteNotes }),
      fields: ['favorite'],
    })
  },
}

const mutations = {
  setLoggedIn(state, value) {
    state.isLoggedIn = value
  },
  setData(state, data) {
    state.data = data
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
