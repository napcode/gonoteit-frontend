const state = {
  isOffline: true,
  isLoading: false,
  isMobile: window.innerWidth < 1024,
  runningTasks: [],
}

const getters = {
  timestamp() {
    return parseInt(window.localStorage.getItem('timestamp'))
  },
}

const actions = {
  reloadAppData() {},
  loadAppDataFromCache({ commit }) {
    const notes = JSON.parse(window.localStorage.getItem('notes'))
    if (notes) {
      commit('notes/setNotes', notes, { root: true })
    }
    const userData = JSON.parse(window.localStorage.getItem('userData'))
    if (userData) {
      commit('user/setData', userData, { root: true })
    }
  },
  clearCache() {
    window.localStorage.removeItem('notes')
    window.localStorage.removeItem('userData')
  },
  updateIsMobile({ commit }) {
    const isMobile = window.innerWidth < 1024
    commit('setIsMobile', isMobile)
  },
}

const mutations = {
  setIsOffline(state, value) {
    state.isOffline = value
  },
  startTask(state, task) {
    state.runningTasks = [task, ...state.runningTasks]
    state.isLoading = true
  },
  endTask(state, task) {
    let taskIndex = state.runningTasks.indexOf(task)
    if (taskIndex > -1) {
      state.runningTasks.splice(taskIndex, 1)
      state.runningTasks = [...state.runningTasks]
      state.isLoading = state.runningTasks.length > 0
    }
  },
  setTimestamp(state, timestamp) {
    window.localStorage.setItem('timestamp', timestamp)
  },
  setIsMobile(state, value) {
    state.isMobile = value
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
