import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import modal from './modal'
import notes from './notes'
import user from './user'
import app from './app'

export default new Vuex.Store({
  modules: {
    modal,
    notes,
    user,
    app,
  },
})
