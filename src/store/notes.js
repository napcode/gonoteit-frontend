import api from '@/api'

import { DateTime } from 'luxon'
import sortBy from 'lodash.sortby'
import last from 'lodash.last'
import { NoteType } from '@/enums.js'

function addTodayAndTomorrow(days, datetime) {
  const today = DateTime.local().startOf('day')
  const tomorrow = DateTime.local().plus({ days: 1 }).startOf('day')

  if ((days.length === 0 || last(days).datetime < today) && today < datetime) {
    days.push({
      datetime: today,
      date: today.toFormat('dd-MM-yyyy'),
      notes: [],
    })
  }

  if ((days.length === 0 || last(days).datetime < tomorrow) && tomorrow < datetime) {
    days.push({
      datetime: tomorrow,
      date: tomorrow.toFormat('dd-MM-yyyy'),
      notes: [],
    })
  }
}

function today() {
  return DateTime.local().startOf('day').toSeconds()
}

const state = {
  notes: [],
  enabledFilters: [],
  searchPhrase: '',
  defaultTop: 10,
  top: 10,
  filters: {
    calendarNotes: n => n.date !== null,
    notCalendarNotes: n => n.date === null,
    pastNotes: n => (n.date < today() ? n.date !== null : false),
    notPastNotes: n => (n.date >= today() ? n.date !== null : false),
    notReccuringNotes: n => n.type !== NoteType.RECURRING_NOTE.name,
  },
  availableFilters: [],
}

const getters = {
  days(state, { searchedNotes }) {
    const calendarNotes = searchedNotes.filter(state.filters['calendarNotes'])
    const sortedNotes = sortBy(calendarNotes, 'date')
    const activeNotes = state.top ? sortedNotes.slice(0, state.top) : sortedNotes

    return activeNotes.reduce((days, note) => {
      const day = DateTime.fromSeconds(note.date).toFormat('dd-MM-yyyy')
      const datetime = DateTime.fromFormat(day, 'dd-MM-yyyy')

      addTodayAndTomorrow(days, datetime)

      if (days.length > 0 && day === last(days).date) {
        last(days).notes.push(note)
      } else {
        days.push({
          datetime,
          date: day,
          notes: [note],
        })
      }

      return days
    }, [])
  },
  months(state, { days }) {
    const months = []
    for (const day of days) {
      const month = day.datetime.toFormat('LLLL yyyy')
      const monthId = day.datetime.toFormat('LL-yyyy')
      const datetime = DateTime.fromFormat(month, 'LLLL yyyy')
      if (months.length > 0 && month === last(months).name) {
        last(months).days.push(day)
      } else {
        if (months.length > 0) {
          let nextMonth = last(months).datetime.plus({ months: 1 })
          while (month !== nextMonth.toFormat('LLLL yyyy')) {
            months.push({
              name: nextMonth.toFormat('LLLL yyyy'),
              id: nextMonth.toFormat('LL-yyyy'),
              datetime: nextMonth,
              days: [],
            })
            nextMonth = nextMonth.plus({ months: 1 })
          }
        }
        months.push({
          name: month,
          id: monthId,
          datetime,
          days: [day],
        })
      }
    }

    return months
  },
  filteredNotes(state) {
    return state.enabledFilters.reduce(
      (notes, filter) => notes.filter(state.filters[filter]),
      state.notes,
    )
  },
  searchedNotes(state, { filteredNotes }) {
    const searchString = state.searchPhrase.toUpperCase()
    if (searchString === '') {
      return filteredNotes
    }

    return filteredNotes.filter(note => {
      const title = note.data['title']
      const content = note.data['content']
      if (title.toUpperCase().includes(searchString)) {
        return true
      }
      if (content.toUpperCase().includes(searchString)) {
        return true
      }

      return false
    })
  },
  activeNotes(state, { searchedNotes }) {
    if (state.top == null) {
      return searchedNotes
    }

    return searchedNotes.slice(0, state.top)
  },
}

const actions = {
  reloadAllNotes({ commit }) {
    commit('app/startTask', 'reload-all-notes', { root: true })
    api
      .getNotes()
      .then(response => {
        if ('errors' in response.data) {
          console.error(response.data.errors)
        } else {
          commit('setNotes', response.data.data.allEntities)
          commit('app/setTimestamp', response.data.data.timestamp, { root: true })
        }
      })
      .catch(error => {
        console.error(error)
      })
      .finally(() => commit('app/endTask', 'reload-all-notes', { root: true }))
  },
  getChangelog({ commit, rootGetters }) {
    commit('app/startTask', 'get-changelog', { root: true })
    const timestamp = rootGetters['app/timestamp']
    api
      .getChangelog({ timestamp })
      .then(response => {
        if ('errors' in response.data) {
          console.error(response.data.errors)
        } else {
          const changelog = response.data.data.changelog
          commit('app/setTimestamp', changelog.timestamp, { root: true })
          commit('addNotes', changelog.created)
          commit('updateNotes', changelog.updated)
          commit('deleteNotes', changelog.deleted)
        }
      })
      .catch(error => {
        console.error(error)
      })
      .finally(() => commit('app/endTask', 'get-changelog', { root: true }))
  },
}

const mutations = {
  setNotes(state, notes) {
    state.notes = notes
  },
  addNote(state, note) {
    state.notes = [note, ...state.notes]
  },
  addNotes(state, notes) {
    state.notes = [...notes, ...state.notes]
  },
  updateNote(state, note) {
    state.notes = [note, ...state.notes.filter(n => n.id !== note.id)]
  },
  updateNotes(state, notes) {
    const notesIds = notes.map(n => n.id)
    state.notes = [...notes, ...state.notes.filter(n => !notesIds.includes(n.id))]
  },
  deleteNote(state, noteId) {
    state.notes = state.notes.filter(n => n.id !== noteId)
  },
  deleteNotes(state, notesIds) {
    state.notes = state.notes.filter(n => !notesIds.includes(n.id))
  },
  enableFilter(state, filter) {
    state.enabledFilters = [filter, ...state.enabledFilters]
  },
  enableFilters(state, filters) {
    state.enabledFilters = [...filters, ...state.enabledFilters]
  },
  disableFilter(state, filter) {
    state.enabledFilters = [...state.enabledFilters.filter(f => f != filter)]
  },
  setFilters(state, filters) {
    state.enabledFilters = [...filters]
  },
  resetFilters(state) {
    state.enabledFilters = []
  },
  setSearchPhrase(state, phrase) {
    state.searchPhrase = phrase
  },
  setAvailableFilter(state, filters) {
    state.availableFilters = filters
  },
  setTop(state, value) {
    state.top = value
  },
  resetTop(state) {
    state.top = null
  },
  setDefaultTop(state) {
    state.top = state.defaultTop
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
